package com.arcao.geocaching.api.configuration;

import com.arcao.geocaching.api.configuration.OAuthGeocachingApiConfiguration;

public class PublicConfigurationClass implements OAuthGeocachingApiConfiguration {

  public String getApiServiceEntryPointUrl() {
    return null;
  }

  public String getConsumerKey() {
    return null;
  }

  public String getConsumerSecret() {
    return null;
  }

  public String getOAuthAuthorizeUrl() {
    return null;
  }

  public String getOAuthRequestUrl() {
    return null;
  }

  public String getOAuthAccessUrl() {
    return null;
  }

}
